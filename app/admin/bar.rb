ActiveAdmin.register Bar do
  scope_to :current_admin_user

  permit_params :address1, :address2, :phone_number, :website, :name, :avatar, :video, :outside_image, :inside_image1, :inside_image2, :inside_image3, :bar_type, :music_type, :age_demographic, :tagline, :about_us, promos_attributes: [:id, :name, :description, :start_date, :end_date, :_destroy]

  #actions :all, :except => [:new]

  filter :name

  BAR_TYPES = ['Dive', 'Sports', 'Karaoke', 'Comedy', 'Dance', 'Hotel', 'Biker', 'Wine', 'Brew Pub', 'Gay', 'Restaurant', 'Topless']
  MUSIC_TYPES = ['Rock', 'Hip Hop', 'Pop', 'Techno', 'Classical', 'Amateur']
  AGE_DEMOGRAPHICS = ['21-24', '24-28', '28-33', '33-39', '40+']

  index download_links: false, title: "Your Bars and Promotions", as: :grid, columns: 5 do |bar|
    h3 link_to bar.name, admin_bar_path(bar)
    ul do 
      bar.promos.map do |promo|
        li link_to promo.name, admin_bar_promo_path(bar, promo)
      end
    end
  end


  form do |f|
    f.inputs "Bar Information" do
      f.input :name
      f.input :address1
      f.input :address2
      f.input :phone_number
      f.input :website
      f.input :tagline
      f.input :avatar, as: :file, hint: "current image: #{image_tag(f.object.avatar.url(:thumb))}".html_safe
      f.input :video, as: :file, hint: "current video: #{image_tag(f.object.video.url(:thumb))}".html_safe
      f.input :outside_image, as: :file, hint: "current image: #{image_tag(f.object.outside_image.url(:thumb))}".html_safe
      f.input :inside_image1, as: :file, hint: "current image: #{image_tag(f.object.inside_image1.url(:thumb))}".html_safe
      f.input :inside_image2, as: :file, hint: "current image: #{image_tag(f.object.inside_image2.url(:thumb))}".html_safe
      f.input :inside_image3, as: :file, hint: "current image: #{image_tag(f.object.inside_image3.url(:thumb))}".html_safe
      f.input :bar_type, label: 'Bar Type', as: :select, collection: BAR_TYPES, include_blank: false
      f.input :music_type, label: 'Music', as: :select, collection: MUSIC_TYPES
      f.input :age_demographic, label: 'Age Demographic', as: :select, collection: AGE_DEMOGRAPHICS
      f.input :about_us
    end
    f.actions
  end

  show do |bar|
    attributes_table do
      row :name
      row :avatar do
        image_tag bar.avatar.url(:thumb)
      end
      row :video do
        image_tag bar.video.url(:thumb)
      end
      row :outside_image do
        image_tag bar.outside_image.url(:thumb)
      end
      row :inside_image1 do
        image_tag bar.inside_image1.url(:thumb)
      end
      row :inside_image2 do
        image_tag bar.inside_image2.url(:thumb)
      end
      row :inside_image3 do
        image_tag bar.inside_image3.url(:thumb)
      end
      row :address1
      row :address2
      row :phone_number
      row :website
      row :bar_type
      row :music_type 
      row :age_demographic
      row :tagline
      row :about_us
    end

    h3 "#{link_to 'Promotions', admin_bar_promos_path(bar)}".html_safe
    table_for(bar.promos) do |promo|
      column :name do |promo|
        link_to(promo.name, admin_bar_promo_path(promo.bar_id, promo.id))
      end
      column :description
      column :start_date
      column :start_time do |promo|
        promo.start_time.strftime("%I:%M%p")
      end
      column :end_date
      column :end_time do |promo|
        promo.end_time.strftime("%I:%M%p")
      end
    end
  end


end
