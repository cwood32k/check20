ActiveAdmin.register Promo do
  belongs_to :bar

  permit_params :bar_id, :name, :image, :description, :start_date, :end_date, :start_time, :end_time

  filter :name
  PROMO_CATEGORIES = ["Alcohol", "Food", "Event"]

  index download_links: false do
  	column :name do |promo|
      link_to promo.name, admin_bar_promo_path(promo.bar_id, promo)
    end
    column :category
  	column :description
  	column :start_date
    column :start_time do |promo|
      promo.start_time.strftime("%I:%M%p")
    end
  	column :end_date 
    column :end_time do |promo|
      promo.end_time.strftime("%I:%M%p")
    end
  end

  show do |promo|
    attributes_table do
      row :bar do
        link_to promo.bar.name, admin_bar_path(promo.bar)
      end
      row :name
      row :image do
        image_tag promo.image.url(:thumb)
      end
      row :category
      row :coupon_image do 
        image_tag promo.coupon_image.url(:thumb)
      end
      row :description
      row :start_date
      row :start_time do
        promo.start_time.strftime("%I:%M%p")
      end
      row :end_date
      row :end_time do
        promo.end_time.strftime("%I:%M%p")
      end
    end
  end


  form do |f|
  	f.inputs "Promo Information" do
  		f.input :name
      f.input :image, as: :file, hint: "current image: #{image_tag(f.object.image.url(:thumb))}".html_safe
      f.input :coupon_image, as: :file, hint: "current image: #{image_tag(f.object.coupon_image.url(:thumb))}".html_safe
  		f.input :category, as: :select, include_blank: false, :collection => PROMO_CATEGORIES
      f.input :description
  		f.input :start_date, as: :date_picker
  		f.input :end_date, as: :date_picker
      f.input :start_time, as: :time_picker
      f.input :end_time, as: :time_picker
      f.actions
  	end
  end

end
