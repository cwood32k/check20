# ActiveAdmin.register User do
# 	# not even admin can edit users
# 	#
# 	permit_params do
#    		permitted = [:id, :provider, :uid, :email, :username, :dob, :female, :first_name, :last_name, :password, :password_confirmation]
#    		permitted
# 	end

# 	index do
# 		id_column
# 		column :provider
# 		column :uid
# 		column :email
# 		column :username
# 		column :dob
# 		column :female
# 		column :sign_in_count
# 		column :last_sign_in_at
# 		column :created_at
# 		column :updated_at
# 	end

# 	form do |f|
# 		f.inputs "User Information" do
# 			f.input :provider
# 			f.input :uid
# 			f.input :email
# 			f.input :username
# 			f.input :password
#       		f.input :password_confirmation
# 			f.input :first_name
# 			f.input :last_name
# 			f.input :dob
# 			f.input :female
# 		end
# 		f.actions
# 	end

# 	filter :email
# 	filter :username
# 	filter :dob
# 	filter :provider

# 	sidebar :help do
# 		ul do
# 			li "These are the users who signed up thru the Check20 mobile apps"
# 			li "Look at all their statistics"
# 		end
# 	end

# end
