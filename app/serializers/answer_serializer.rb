class AnswerSerializer < ActiveModel::Serializer
	attributes :question_id, :user_id, :choice, :question_num

	def question_num
		object.question.number
	end


end