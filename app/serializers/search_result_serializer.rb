class SearchResultSerializer < ActiveModel::Serializer
	attributes :type, :id, :image_path, :title, :info, :bar_data, :promo_data

	def bar_data
		BarSerializer.new(object.bar, scope: scope, root: false).to_json
	end

	def promo_data
		PromoSerializer.new(object.promo, scope: scope, root: false).to_json
	end
end