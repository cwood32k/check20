class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :username, :dob, :auth_token, :gender, :num_answers

  def gender
  	object.female ? "F" : "M"
  end

  def dob
  	object.dob.strftime("%m/%d/%Y")
  end

end
