class BarSerializer < ActiveModel::Serializer
  attributes :id, :male_followers, :female_followers, :following, :male_check_ins, :female_check_ins, :checked_in, :male_likes, :female_likes, :liked, :address1, :address2, :phone_number, :website, :name, :avatar_path, :video_path, :video_thumb_path, :outside_image_path, :inside_image1_path, :inside_image2_path, :inside_image3_path, :bar_type, :music_type, :age_demographic, :tagline, :about_us

  def avatar_path
  	object.avatar.url(:thumb)
  end

  def video_path
    object.video.url
  end

  def video_thumb_path
    object.video.url(:thumb)
  end

  def outside_image_path
  	object.outside_image.url(:medium)
  end

  def inside_image1_path
  	object.inside_image1.url(:medium)
  end

  def inside_image2_path
  	object.inside_image2.url(:medium)
  end

  def inside_image3_path
  	object.inside_image3.url(:medium)
  end

  def phone_number
    object.phone_number.insert(3, '-')
    object.phone_number.insert(7, '-')
    object.phone_number
  end

  def following
    scope.bars.include?(object) ? true : false
  end

  def liked
    scope.likes.active.map(&:bar_id).include?(object.id)
  end

  def checked_in
    scope.check_ins.active.map(&:bar_id).include?(object.id)
  end

  def male_followers
    object.male_followers
  end

  def female_followers
    object.female_followers
  end

  def male_check_ins
    object.male_check_ins
  end

  def female_check_ins
    object.female_check_ins
  end

  def male_likes
    object.male_likes
  end

  def female_likes
    object.female_likes
  end

end
