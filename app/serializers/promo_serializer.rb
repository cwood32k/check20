class PromoSerializer < ActiveModel::Serializer

	attributes :id, :name, :category, :bar_name, :bar_id, :image_path, :coupon_image_path, :description, :start_date_relative, :end_date_relative

	def bar_name
		object.bar.name
	end

	def bar_id
		object.bar.id
	end

	def image_path
		object.image.url(:thumb)
	end

	def coupon_image_path
		object.coupon_image.url(:medium)
	end

	def start_date_relative
		object.start_date_relative
	end

	def end_date_relative
		object.end_date_relative
	end

end