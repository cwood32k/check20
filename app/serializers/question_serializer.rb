class QuestionSerializer < ActiveModel::Serializer

	attributes :id, :question, :number, :answer1, :answer2, :answer3, :answer4, :user_choice


	def user_choice
		if scope[1][:action] == "index"
			if answer = scope[0].answers.where(question_id: object.id).first
				answer.choice
			else
				nil
			end
		end
	end

end