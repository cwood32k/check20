require 'houston'

class NotificationManager
	APN = Houston::Client.development
	APN.certificate = File.read(File.join(Rails.root, "app/apns/apn.pem"))

	def self.push(user, message, category, custom_data = nil)
	    notifications = user.devices.map{ |device|
	      notification = Houston::Notification.new(device: device.token)
				notification.alert = message
				notification.badge = 1
				notification.sound = "default"
				notification.category = category
				notification.custom_data = custom_data if custom_data
				notification
	    }
	    unless notifications.empty?
	    	notifications.each do |notification|
	    		APN.push(notification)
	    	end
	    end
	end

end