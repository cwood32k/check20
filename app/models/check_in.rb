class CheckIn < ActiveRecord::Base
	belongs_to :bar
	belongs_to :user

	scope :active, -> { where("updated_at > ?", 3.hours.ago) }
end
