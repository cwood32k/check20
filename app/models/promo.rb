class Promo < ActiveRecord::Base
  include ActionView::Helpers::DateHelper

	belongs_to :bar

	after_create :send_notifications

	has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/missing_:style.png"
  	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  	has_attached_file :coupon_image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/missing_:style.png"
  	validates_attachment_content_type :coupon_image, :content_type => /\Aimage\/.*\Z/

  scope :active, -> { where("end_date > ? or end_date = ? and end_time > ?", Date.today, Date.today, Time.now) }

  def start_date_relative
    d = start_date
    t = start_time
    dt = DateTime.new(d.year, d.month, d.day, t.hour, t.min, t.sec, Time.zone.now.to_datetime.zone)

    if dt < Time.now
      relative_time = "Started " + distance_of_time_in_words(Time.zone.now, dt) + " ago"
    else
      relative_time = "Starts " + distance_of_time_in_words(Time.zone.now, dt) + " from now"
    end

    relative_time
  end

  def end_date_relative
    d = end_date
    t = end_time
    dt = DateTime.new(d.year, d.month, d.day, t.hour, t.min, t.sec, Time.zone.now.to_datetime.zone)
    if dt < Time.now
      relative_time = "Ended " + distance_of_time_in_words(Time.zone.now, dt) + " ago"
    else
      relative_time = "Ends " + distance_of_time_in_words(Time.zone.now, dt) + " from now"
    end

    relative_time
  end

  def self.string_search(postgresql_regex)
    active.where("regexp_replace(lower(name), '[[:punct:]]', '') ~ ?", "#{postgresql_regex}").map { |result|
      SearchResult.new(type: result.class.to_s, id: result.id, image_path:result.image.url(:thumb), title: "#{result.bar.name}: #{result.name}", info: "#{result.start_date_relative}", promo: Promo.find(result.id))
    }
  end

	def send_notifications
		bar.followings.each do |following|
		NotificationManager.push(following.user, "#{bar.name} added a new promo!", "New Promo", promo_id: id)
		end
	end


end
