class Like < ActiveRecord::Base
	belongs_to :bar
	belongs_to :user

	scope :active, -> { where("updated_at > ?", 1.hour.ago) }
end
