class Bar < ActiveRecord::Base
	belongs_to :admin_user
	has_many :promos, dependent: :destroy
	
	has_many :check_ins
	has_many :likes
  	has_many :followings
  	has_many :beacons

	accepts_nested_attributes_for :promos, :allow_destroy => true

	has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/missing_:style.png"
	validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

	has_attached_file :video, styles: {
	    medium: { format: 'mov', convert_options: { output: { strict: "experimental", vf: "scale=-1:370"} }, streaming: true },
	    thumb: { format: 'jpg', convert_options: { output: { strict: "experimental", vf: "scale=-1:162"} }, time: 1 }
	}, processors: [:ffmpeg, :thumbnail_generator], default_url: "/missing_:style.png", preserve_files: false

	validates_attachment_content_type :video, content_type: ['video/avi','video/quicktime','video/3gpp','video/x-ms-wmv','video/mp4','video/mpeg']

	has_attached_file :outside_image, styles: { medium: "340x162>", thumb: "100x100>" }, default_url: "/missing_:style.png"
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

	has_attached_file :inside_image1, styles: { :medium => "340x162>", :thumb => "100x100>" }, default_url: "/missing_:style.png"
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

	has_attached_file :inside_image2, styles: { :medium => "340x162>", :thumb => "100x100>" }, default_url: "/missing_:style.png"
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

	has_attached_file :inside_image3, styles: { :medium => "340x162>", :thumb => "100x100>" }, default_url: "/missing_:style.png"
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
	
	def self.string_search(postgresql_regex)
		where("regexp_replace(lower(name), '[[:punct:]]', '') ~ ?", "#{postgresql_regex}").map { |result| 
					SearchResult.new(type: result.class.to_s, id: result.id, image_path:result.avatar.url(:thumb), title: result.name, info: "#{result.bar_type}, #{result.music_type}, #{result.age_demographic}", bar: Bar.find(result.id) ) 
				  }
	end

	def male_followers
		count = 0
		followings.map { |follower| count += 1 if !follower.user.female }
		count
	end

	def female_followers
		count = 0
		followings.map { |follower| count += 1 if follower.user.female }
		count
	end

	def male_likes
		count = 0
		likes.active.map {|like| count += 1 if !like.user.female }
		count
	end

	def female_likes
		count = 0
		likes.active.map {|like| count += 1 if like.user.female }
		count
	end

	def male_check_ins
		count = 0
		check_ins.active.map {|like| count += 1 if !like.user.female }
		count
	end

	def female_check_ins
		count = 0
		check_ins.active.map {|like| count += 1 if like.user.female }
		count
	end
	
end
