class SearchResult 
	include ActiveModel::Model
	include ActiveModel::SerializerSupport

	attr_accessor :type, :id, :image_path, :title, :info, :bar, :promo

end