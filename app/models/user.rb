class User < ActiveRecord::Base
	validates :email, uniqueness: true
	validates :auth_token, uniqueness: true
  
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :omniauthable, :omniauth_providers => [:twitter]
	
	devise :database_authenticatable, :registerable,
       :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:username]

  before_create :generate_authentication_token!

  has_many :devices
  has_many :answers
  has_many :questions, through: :answers

  has_many :followings
  has_many :bars, through: :followings
  has_many :promos, through: :bars

  has_many :check_ins
  has_many :likes

 	def generate_authentication_token!
 		begin
 			self.auth_token = Devise.friendly_token
 		end while self.class.exists?(auth_token: auth_token)
 	end

 	def gender=(val)
 		self.female = val == "M" ? false : true
 	end

 	def self.from_omniauth(auth)
 		#logger.debug "#############################twitter response hash#######################################"
 		#logger.debug "#{auth.to_yaml}"
 		where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
 			user.provider = auth.provider
 			user.uid = auth.uid
 			user.username = auth.info.nickname
 		end
 	end

 	def email_required?
 		super && provider.blank?
 	end

 	def password_required?
 		super && provider.blank?
 	end

 	def update_with_password(params, *options)
 		if encrypted_password.blank?
 			update_attributes(params, *options)
 		else
 			super
 		end
 	end

 end