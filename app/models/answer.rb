class Answer < ActiveRecord::Base
	belongs_to :user
	belongs_to :question

	after_create :update_user

	def update_user
		user.num_answers += 1
		user.save!
	end
end
