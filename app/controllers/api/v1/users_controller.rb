class Api::V1::UsersController < Api::V1::BaseApiController
	before_action :authenticate_with_token!, except: [:delete_device, :create]

	def index
		render json: User.all
	end

	def show
		render json: current_user
	end

	def create
		user = User.new(user_params)
		user.dob = Date.strptime(user_params[:dob], '%m/%d/%Y')
		if user.save
			self.status = 201
			render json: user
		else
			self.status = 422
			render json: user.errors
		end
	end

	def update
		current_user.dob = Date.strptime(user_params.delete(:dob), '%m/%d/%Y') if user_params[:dob]
		if user_params[:password] == nil
			user_params.delete(:password)
		else
			if !current_user.valid_password?(params[:current_password])
				self.status = 422
				return render json: { password: "The current password you entered is invalid" }
			end
		end
		if current_user.update(user_params)
			self.status = 200
			render json: current_user
		else
			self.status = 422
			render json: user.errors
		end
	end

	def my_places
		render json: current_user.bars, root: "bars"
	end

	def add_to_my_places
		following = current_user.followings.build
		following.bar_id = params[:bar_id]
		if following.save
			self.status = 200
			self.response_body = []
		else
			self.status = 404
			self.response_body = []
		end
	end

	def remove_from_my_places
		following = current_user.followings.where(bar_id: params[:bar_id])
		if following.destroy_all
			self.status = 200
			self.response_body = []
		else
			self.status = 404
			self.response_body = []
		end
	end

	def register_device

		device = User.first.devices.build

		device.token = params[:device_token]
		if device.save
			self.status = 200
			self.response_body = []
		else
			self.status = 404
			self.response_body = []
		end

	end

	def delete_device
		if Device.where(token: params[:device_token]).destroy_all
			self.status = 200
			self.response_body = []
		else
			self.status = 404
			self.response_body = []
		end
	end

	private

	def user_params
		params.require(:user).permit(:id, :provider, :uid, :email, :username, :dob, :gender, :first_name, :last_name, :password, :password_confirmation)
	end
end