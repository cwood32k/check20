class Api::V1::SessionsController < Api::V1::BaseApiController

	def create
		user_password = params[:session][:password]
		email = params[:session][:email]
		user =  User.find_by(email: email)
		
		if (user && user.valid_password?(user_password))
			user.generate_authentication_token!
			user.save
			self.status = 200
			render json: user
		else
			self.status = 422
			render json: "Invalid email or password"
		end
	end

	def twitter_login
		username = params[:username]
		user = User.find_by(username: username, provider: "twitter")
		if user
			sign_in user, store: false
			user.generate_authentication_token!
			user.save
			self.status = 200
			render json: user
		else

		end
	end

	def destroy
		user = User.find_by(auth_token: params[:id])
		user.generate_authentication_token!
		user.save
		head 204
	end
	
end
