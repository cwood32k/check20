class Api::V1::OmniauthCallbacksController < Devise::OmniauthCallbacksController

	def twitter
		user = User.from_omniauth(request.env["omniauth.auth"])
		logger.debug("user logged in through omniauth: #{user.to_yaml}")
		if user.persisted?
			sign_in_and_redirect user, notice: "Signed in!"
		else
			redirect_to new_user_registration_url
		end
	end

	private

	def user_params
    	params.require(:user).permit(:provider, :uid, :email, :username, :dob, :female, :first_name, :last_name, :password, :password_confirmation)
  	end
end