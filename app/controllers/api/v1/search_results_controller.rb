class Api::V1::SearchResultsController < Api::V1::BaseApiController
	before_action :authenticate_with_token!

	def search
		text = params[:text].downcase.gsub(/[^a-zA-Z0-9\s]/i, '').split
		text.delete("the")
		text.delete("a")
		text.delete("of")
		text.delete("and")
		Rails.logger.debug text
		postgresql_regex = text.reduce("") { |regex, word|  word + "|" + regex }
		postgresql_regex= postgresql_regex.gsub(/\|$/, '')

		results = Bar.string_search(postgresql_regex).sort! {|a, b| (b.title.downcase.gsub(/[^a-zA-Z0-9\s]/i, '').split & text) <=> (a.title.downcase.gsub(/[^a-zA-Z0-9\s]/i, '').split & text)}
		results.concat Promo.string_search(postgresql_regex).sort! {|a, b| (b.title.downcase.gsub(/[^a-zA-Z0-9\s]/i, '').split & text) <=> (a.title.downcase.gsub(/[^a-zA-Z0-9\s]/i, '').split & text)}

		render json: results
	end

end
