class Api::V1::BaseApiController < ActionController::Metal
	
	include AbstractController::Rendering
	include ActionController::Renderers::All
	include ActionController::Serialization

	include ActionController::StrongParameters
	include ActionController::ConditionalGet

	include ActionController::RequestForgeryProtection

	include ActionController::ForceSSL
	include AbstractController::Callbacks

	include ActionController::Instrumentation

	include ActionController::ParamsWrapper

	wrap_parameters format: [:json]

	include Authenticable

	protect_from_forgery with: :null_session

end