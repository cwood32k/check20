class Api::V1::BeaconsController < Api::V1::BaseApiController
	before_action :authenticate_with_token!

	def check_in
		beacon = Beacon.find_by_code(params[:code])
		check_in = CheckIn.find_or_create_by(bar_id: beacon.bar.id, user_id: current_user.id)

		if check_in.touch
			self.status = 201
			self.response_body = []
		else
			self.status = 422
			self.response_body = []
		end
	end
end
