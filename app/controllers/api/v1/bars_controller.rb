class Api::V1::BarsController < Api::V1::BaseApiController
	before_action :authenticate_with_token!

	def index
		render json: Bar.all
	end

	def check_in
		check_in = CheckIn.find_or_create_by(bar_id: Bar.find(params[:id]), user_id: current_user.id)

		if check_in.touch
			self.status = 201
			self.response_body = []
		else
			self.status = 422
			self.response_body = []
		end
	end

	def like
		like = Like.find_or_create_by(bar_id: Bar.find(params[:id]), user_id: current_user.id)

		if like.touch
			self.status = 201
			self.response_body = []
		else
			self.status = 422
			self.response_body = []
		end
	end

	def show
		render json: Bar.find(params[:id])
	end

end
