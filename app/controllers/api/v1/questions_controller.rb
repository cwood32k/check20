class Api::V1::QuestionsController < Api::V1::BaseApiController
	before_action :authenticate_with_token!

	def next_question

		q = Question.find_by_number(current_user.num_answers + 1)
		
		if q.present?
			render json: q, scope: [current_user, params]
			return
		end

		raise ActionController::RoutingError.new("Question invalid")

	end

	def index
		render json: Question.all, scope: [current_user, params]
	end

	def show
		q = Question.find(params[:id])

		if q.present?
			render json: q, scope: [current_user, params]
			return
		end

		raise ActionController::RoutingError.new("Question invalid")
	end

	def save_answer
		q = Question.find_by_number(params[:answer][:question_num].to_i)
		a = Answer.where(user_id: current_user.id, question_id: q.id).first_or_create
		a.choice = params[:answer][:choice]

		if a.save
			self.status = 201
			render json: {answer: a}
		else
			self.status = 500
			self.response_body = []
		end
	end

	def review
		if params[:number].to_i <= current_user.num_answers
			q = Question.find_by_number(params[:number].to_i)
			
			if q.present?
				render json: q, scope: [current_user, params]
				return
			end
		end
		raise ActionController::RoutingError.new("Question invalid")
	end

end
