class Api::V1::PromosController < Api::V1::BaseApiController
	before_action :authenticate_with_token!

	def index
		render json: current_user.promos.active.order('category asc, promos.id desc'), root: "promos"
	end

	def show
		render json: Promo.find(params[:id])
	end

	def bar_promos
		render json: Promo.active.where(bar_id: params[:id])
	end
end