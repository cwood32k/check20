class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery

  before_filter :update_sanitized_params, if: :devise_controller?

  def update_sanitized_params
    #these parameters will be passed to Devise on registration
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:email, :dob, :female, :username, :password, :password_confirmation)}
  end
  
  def authenticate_admin_user!
  	raise SecurityError unless current_admin_user.is_a?(AdminUser)
  end

  def after_sign_out_path_for(resource_or_scope)
    case resource_or_scope
      when :admin_user, AdminUser
        new_admin_user_session_path
      else
        super
    end
  end
end
