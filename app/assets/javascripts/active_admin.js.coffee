#= require active_admin/base
$ ->
  $(document).on 'focus', '.datepicker:not(.hasDatepicker)', ->
    $(@).datepicker dateFormat: 'yy-mm-dd'