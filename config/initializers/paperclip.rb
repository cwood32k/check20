if Rails.env.development?
	Paperclip.options[:command_path] = "/usr/local/bin/"
end

if Rails.env.production?
	Paperclip::Attachment.default_options[:url] = ':s3_domain_url'
	Paperclip::Attachment.default_options[:path] = '/:class/:attachment/:id_partition/:style/:filename'
end