require 'api_constraints'

Rails.application.routes.draw do
  
  namespace :api, defaults: { format: :json }, 
                              #constraints: { subdomain: 'api' },
                                path: '/' do
    scope module: :v1,
                  constraints: ApiConstraints.new(version: 1, default: true) do
        resources :users, :only => [:index, :show, :create, :update]
        resources :sessions, :only => [:create, :destroy]
        resources :bars, :only => [:index, :show]
        resources :promos, :only => [:index, :show]
        resources :answers, :only => [:show]

        post "register_device" => "users#register_device"
        delete "register_device" => "users#delete_device"

        get "/search" => "search_results#search"

        get "my_places" => "users#my_places"
        post "update_my_places" => "users#add_to_my_places"
        delete "update_my_places" => "users#remove_from_my_places"

        get "next_question" => "questions#next_question"
        get "question/:id" => "questions#show"
        get "questions" => "questions#index"
        post "save_answer" => "questions#save_answer"

        get "bars/:id/promos" => "promos#bar_promos"

        post "bars/like/:id" => "bars#like"
        post "bars/check_in/:id" => "bars#check_in"
        post "beacons/check_in/:code" => "beacons#check_in"
        
        post "twitter_login" => "sessions#twitter_login", :as => "twitter_login"

    end
  end

  devise_for :users, :controllers => { :omniauth_callbacks => "api/v1/omniauth_callbacks" }
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root to: "home_page#home"
end
