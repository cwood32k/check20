class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|

    	t.integer :bar_id
    	t.integer :user_id

      t.timestamps
    end
    add_index :likes, [:user_id, :bar_id], unique: true
    add_index :likes, :created_at, unique: false
  end
end
