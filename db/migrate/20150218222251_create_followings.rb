class CreateFollowings < ActiveRecord::Migration
  def change
    create_table :followings do |t|
      t.integer :bar_id
      t.integer :user_id

      t.timestamps
    end
    add_index :followings, :user_id, unique: false
    add_index :followings, :bar_id, unique: false
  end
end
