class CreateBeacons < ActiveRecord::Migration
  def change

    create_table :beacons do |t|
    	t.integer :bar_id
    	t.integer :code
      t.timestamps
    end

    add_index :beacons, :bar_id, unique: false

  end
end
