class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|

    	t.string 	:question

    	t.integer :number

    	t.string 	:answer1
    	t.string 	:answer2
    	t.string 	:answer3
    	t.string 	:answer4

      t.timestamps
    end

    add_index :questions, :number, unique: true
  end
end
