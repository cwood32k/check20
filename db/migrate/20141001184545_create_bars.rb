class CreateBars < ActiveRecord::Migration
  def change
    create_table :bars do |t|
      t.integer :admin_user_id
      
      t.string :address1
      t.string :address2
      t.string :phone_number
      t.string :website
      t.string :name
      t.has_attached_file :avatar
      t.has_attached_file :video
      t.has_attached_file :outside_image
      t.has_attached_file :inside_image1
      t.has_attached_file :inside_image2
      t.has_attached_file :inside_image3
      t.string :tagline
      t.string :bar_type
      t.string :music_type
      t.string :age_demographic
      t.text :about_us

      t.timestamps
    end
  end
end
