class CreatePromos < ActiveRecord::Migration
  def change
    create_table :promos do |t|
    	
      t.integer 	:bar_id
      t.string 		:name
      t.string    :category
      t.text 	 	:description
      t.has_attached_file :image
      t.has_attached_file :coupon_image
      t.date 	:start_date
      t.date 	:end_date
      t.time :start_time
      t.time :end_time

    end
    
    add_index :promos, :bar_id, unique: false
  end
end
