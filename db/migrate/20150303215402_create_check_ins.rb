class CreateCheckIns < ActiveRecord::Migration
  def change
    create_table :check_ins do |t|

    	t.integer :bar_id
    	t.integer :user_id

      t.timestamps
    end
    add_index :check_ins, [:user_id, :bar_id], unique: true
    add_index :check_ins, :created_at, unique: false
  end
end
