admin =  AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
user1 = User.create(female: false, dob: 27.years.ago, :username => "cwood", :email => "test1@example.com", :password => "America", :password_confirmation => "America")
user2 = User.create(female: true, dob: 27.years.ago, :username => "sally", :email => "test2@example.com", :password => "America", :password_confirmation => "America")
user3 = User.create(female: false, dob: 27.years.ago, :username => "lindsey", :email => "test3@example.com", :password => "America", :password_confirmation => "America")
user4 = User.create(female: true, dob: 27.years.ago, :username => "jill", :email => "test4@example.com", :password => "America", :password_confirmation => "America")
user5 = User.create(female: true, dob: 27.years.ago, :username => "toni", :email => "test5@example.com", :password => "America", :password_confirmation => "America")

10.times do |i|
	bar = Bar.new(:admin_user_id => admin.id, :phone_number => "9093743958", :website => "www.google.com", :age_demographic => '24-28', :bar_type => "Dive", :music_type => "Techno", :about_us => "That welcoming vibe, value and quality has made us a favorite neighborhood bar. Visitors come to us to be part of the artistic, eclectic scene and there is no better place to have a drink and people watch than at our spot.")
	bar.avatar = File.open("public/avatar.png")
	bar.outside_image = File.open("public/outside_#{(i)%5 + 1}.jpeg")
	bar.inside_image1 = File.open("public/inside1.jpg")
	bar.inside_image2 = File.open("public/inside2.jpg")
	bar.inside_image3 = File.open("public/inside3.jpg")

	case i
	when 0
		bar.name = "James Beach"
		bar.tagline = "Drink by the beach"
		bar.address1 = "60 N Venice Blvd"
		bar.address2 = "Venice, CA 90291"
		bar.followings.build(user_id: user2.id)
		bar.followings.build(user_id: user3.id)
		bar.beacons.build(code: 805257)
	when 1
		bar.name = "Spike's Bar & Billiards"
		bar.tagline = "Laid back fun"
		bar.address1 = "7813 Garvey Ave"
		bar.address2 = "Rosemead, CA 91770"
	when 2
		bar.name = "Verdugo"
		bar.tagline = "Upscale nightlife"
		bar.address1 = "3408 Verdugo Rd"
		bar.address2 = "Los Angeles, CA 90065"
	when 3
		bar.name = "Energy Karaoke"
		bar.tagline = "Come sing with us"
		bar.address1 = "33 E Valley Blvd #210"
		bar.address2 = "Alhambra, CA 91801"
		bar.followings.build(user_id: user3.id)
		bar.followings.build(user_id: user4.id)
		bar.followings.build(user_id: user5.id)
	when 4
		bar.name = "Bodega"
		bar.tagline = "Your new favorite spot"
		bar.address1 = "260 E Colorado Blvd #208"
		bar.address2 = "Pasadena, CA 91101"
	when 5
		bar.name = "Freddie's 35er"
		bar.tagline = "Get fucked up!"
		bar.address1 = "12 E Colorado Blvd"
		bar.address2 = "Pasadena, CA 91105"
		bar.followings.build(user_id: user2.id)
		bar.followings.build(user_id: user3.id)
		bar.followings.build(user_id: user5.id)
	when 6
		bar.name = "R Bar"
		bar.tagline = "Come relax with cool cats"
		bar.address1 = "3331 W 8th St"
		bar.address2 = "Los Angeles, CA 90005"
	when 7
		bar.name = "Lazaro's Latin Lounge"
		bar.tagline = "Loungue with us"
		bar.address1 = "1951 Vermont Avenue"
		bar.address2 = "Los Angeles, CA 90007"
	when 8
		bar.name = "Mom's Bar"
		bar.tagline = "Best spot on the westside"
		bar.address1 = "12238 Santa Monica Blvd"
		bar.address2 = "Los Angeles, CA 90025"
		bar.followings.build(user_id: user2.id)
		bar.followings.build(user_id: user3.id)
		bar.followings.build(user_id: user4.id)
		bar.followings.build(user_id: user5.id)
	when 9
		bar.name = "Ray's & Stark Bar"
		bar.tagline = "People get crazy here"
		bar.address1 = "5905 Wilshire Blvd"
		bar.address2 = "Los Angeles, CA 90036"
	end

	bar.followings.build(user_id: user1.id)
		

	bar.save!

	bar.promos.create(:name => "Beer promo", :category => "Alcohol", :image => File.open("public/promo.jpg"), :coupon_image => File.open("public/coupon_code.png"), :description => "free beer. come into our bar any time in the next week and get your first beer free!", :start_date => Date.new(2015,3,7), :start_time => Time.new(2015, 3, 7, 12, 0,0, "+00:00"), :end_date => Date.new(2015,3,8), :end_time => Time.new(2015, 3, 8, 12, 0, 0, "+00:00"))
	bar.promos.create(:name => "Pizza promo", :category => "Food", :image => File.open("public/promo.jpg"), :coupon_image => File.open("public/coupon_code.png"), :description => "free pizza. come into our bar any time in the next week and get a free jumbo pizza slice!", :start_date => Date.new(2015,3,7), :start_time => Time.new(2015, 3, 7, 17, 0,0, "+00:00"), :end_date => Date.new(2015,3,8), :end_time => Time.new(2015, 3, 8, 21, 30, 0, "+00:00"))
	bar.promos.create(:name => "Vodka promo", :category => "Alcohol", :image => File.open("public/promo.jpg"), :coupon_image => File.open("public/coupon_code.png"), :description => "free vodka. come into our bar any time in the next week and get your first shot free!", :start_date => Date.new(2015,3,7), :start_time => Time.new(2015, 3, 7, 18, 0,0, "+00:00"), :end_date => Date.new(2015,3,8), :end_time => Time.new(2015, 3, 8, 23, 0, 0, "+00:00"))
	bar.promos.create(:name => "Fight promo", :category => "Event", :image => File.open("public/promo.jpg"), :coupon_image => File.open("public/coupon_code.png"), :description => "We are showing tonight's big UFC fight. Just come in and enjoy watching with your friends!", :start_date => Date.new(2015,3,7), :start_time => Time.new(2015, 3, 7, 18, 0,0, "+00:00"), :end_date => Date.new(2015,3,8), :end_time => Time.new(2015, 3, 8, 23, 0, 0, "+00:00"))

	bar.save!
end

Question.create(
		number: 1,
		question: "Which of the following do you drink most often?",
		answer1: "Shots",
		answer2: "Beer",
		answer3: "Wine",
		answer4: "Mixed Drinks"
	)

Question.create(
		number: 2,
		question: "What do you eat when you're drinking?",
		answer1: "Food Trucks",
		answer2: "Pizza",
		answer3: "Gourmet",
		answer4: "I don't eat"
	)

Question.create(
		number: 3,
		question: "Last time I was drunk I was..",
		answer1: "Spitting game",
		answer2: "Dancing",
		answer3: "Watching sports",
		answer4: "Hanging out with friends"
	)

Question.create(
		number: 4,
		question: "The musical atmosphere I prefer is..",
		answer1: "Loud and out of control",
		answer2: "Loud but can still have a conversation",
		answer3: "Noticeable but not loud",
		answer4: "Soothing and in the background"
	)

Question.create(
		number: 5,
		question: "I usually go out drinking..",
		answer1: "1-2 times per year",
		answer2: "1-2 times per month",
		answer3: "1-2 times per week",
		answer4: "Everyday"
	)

Question.create(
		number: 6,
		question: "How many times have you thrown up as a result of drinking?",
		answer1: "0",
		answer2: "1-2",
		answer3: "3-10",
		answer4: "10+"
	)

Question.create(
		number: 7,
		question: "How many drinks does it take for you to feel drunk?",
		answer1: "2",
		answer2: "4",
		answer3: "8",
		answer4: "12 or more"
	)

Question.create(
		number: 8,
		question: "How many relationships have you had that started at a bar or club?",
		answer1: "0",
		answer2: "1",
		answer3: "2",
		answer4: "3 or more"
	)

Question.create(
		number: 9,
		question: "How many times have you been kicked out of the venue?",
		answer1: "0",
		answer2: "1",
		answer3: "2",
		answer4: "3 or more"
	)

Question.create(
		number: 10,
		question: "What time does your night usually start?",
		answer1: "6pm",
		answer2: "8pm",
		answer3: "10pm",
		answer4: "12am"
	)

Question.create(
		number: 11,
		question: "How much are you willing to spend on a typical night out?",
		answer1: "$5",
		answer2: "$20",
		answer3: "$50",
		answer4: "I always make it rain"
	)

Question.create(
		number: 12,
		question: "How deep is your crew, typically?",
		answer1: "2-3",
		answer2: "4-10",
		answer3: "11+",
		answer4: "I roll dolo"
	)

Question.create(
		number: 13,
		question: "Which of these genres of music do you prefer most?",
		answer1: "Hip hop",
		answer2: "Techno",
		answer3: "Pop",
		answer4: "Rock n roll"
	)

Question.create(
		number: 14,
		question: "In social settings, how do you prefer to dress?",
		answer1: "Formal",
		answer2: "Casual",
		answer3: "Trendy",
		answer4: "Sexy"
	)

Question.create(
		number: 15,
		question: "How far are you willing to travel to a nighttime destination?",
		answer1: "5 miles",
		answer2: "10 miles",
		answer3: "20 miles",
		answer4: "30 miles or more"
	)

Question.create(
		number: 16,
		question: "Which of these liquors do you prefer?",
		answer1: "Vodka",
		answer2: "Tequila",
		answer3: "Gin",
		answer4: "Whiskey"
	)

Question.create(
		number: 17,
		question: "Which of these beers do you prefer?",
		answer1: "Coors Light",
		answer2: "Guinness",
		answer3: "Blue Moon",
		answer4: "Heineken"
	)

Question.create(
		number: 18,
		question: "Which of these liquors do you prefer?",
		answer1: "Cognac",
		answer2: "Rum",
		answer3: "Brandy",
		answer4: "Jägermeister"
	)

Question.create(
		number: 19,
		question: "What type of venues do you like to hangout at most?",
		answer1: "Bars",
		answer2: "Clubs",
		answer3: "Strip clubs",
		answer4: "Lounges"
	)

Question.create(
		number: 20,
		question: "To get inside, how long are you willing to wait in line?",
		answer1: "0 minutes",
		answer2: "10 minutes",
		answer3: "30 minutes",
		answer4: "1 hour or more"
	)