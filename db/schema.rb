# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150304083220) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "answers", force: true do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.integer  "choice"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "answers", ["user_id"], name: "index_answers_on_user_id", using: :btree

  create_table "bars", force: true do |t|
    t.integer  "admin_user_id"
    t.string   "address1"
    t.string   "address2"
    t.string   "phone_number"
    t.string   "website"
    t.string   "name"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.string   "outside_image_file_name"
    t.string   "outside_image_content_type"
    t.integer  "outside_image_file_size"
    t.datetime "outside_image_updated_at"
    t.string   "inside_image1_file_name"
    t.string   "inside_image1_content_type"
    t.integer  "inside_image1_file_size"
    t.datetime "inside_image1_updated_at"
    t.string   "inside_image2_file_name"
    t.string   "inside_image2_content_type"
    t.integer  "inside_image2_file_size"
    t.datetime "inside_image2_updated_at"
    t.string   "inside_image3_file_name"
    t.string   "inside_image3_content_type"
    t.integer  "inside_image3_file_size"
    t.datetime "inside_image3_updated_at"
    t.string   "tagline"
    t.string   "bar_type"
    t.string   "music_type"
    t.string   "age_demographic"
    t.text     "about_us"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "beacons", force: true do |t|
    t.integer  "bar_id"
    t.integer  "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "beacons", ["bar_id"], name: "index_beacons_on_bar_id", using: :btree

  create_table "check_ins", force: true do |t|
    t.integer  "bar_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "check_ins", ["created_at"], name: "index_check_ins_on_created_at", using: :btree
  add_index "check_ins", ["user_id", "bar_id"], name: "index_check_ins_on_user_id_and_bar_id", unique: true, using: :btree

  create_table "devices", force: true do |t|
    t.integer  "user_id"
    t.string   "token"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "devices", ["token"], name: "index_devices_on_token", unique: true, using: :btree
  add_index "devices", ["user_id"], name: "index_devices_on_user_id", using: :btree

  create_table "followings", force: true do |t|
    t.integer  "bar_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "followings", ["bar_id"], name: "index_followings_on_bar_id", using: :btree
  add_index "followings", ["user_id"], name: "index_followings_on_user_id", using: :btree

  create_table "likes", force: true do |t|
    t.integer  "bar_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "likes", ["created_at"], name: "index_likes_on_created_at", using: :btree
  add_index "likes", ["user_id", "bar_id"], name: "index_likes_on_user_id_and_bar_id", unique: true, using: :btree

  create_table "promos", force: true do |t|
    t.integer  "bar_id"
    t.string   "name"
    t.string   "category"
    t.text     "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "coupon_image_file_name"
    t.string   "coupon_image_content_type"
    t.integer  "coupon_image_file_size"
    t.datetime "coupon_image_updated_at"
    t.date     "start_date"
    t.date     "end_date"
    t.time     "start_time"
    t.time     "end_time"
  end

  add_index "promos", ["bar_id"], name: "index_promos_on_bar_id", using: :btree

  create_table "questions", force: true do |t|
    t.string   "question"
    t.integer  "number"
    t.string   "answer1"
    t.string   "answer2"
    t.string   "answer3"
    t.string   "answer4"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "questions", ["number"], name: "index_questions_on_number", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.date     "dob"
    t.boolean  "female"
    t.integer  "num_answers",            default: 0,  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "auth_token",             default: ""
  end

  add_index "users", ["auth_token"], name: "index_users_on_auth_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
