require 'RMagick'

module Paperclip
  class ThumbnailGenerator < Thumbnail

    def initialize(file, options = {}, attachment = nil)
      super
    end

    def make
      if !['avi','quicktime','3gpp','x-ms-wmv','mp4','mpeg', 'mov'].include?(@format)
        src = Magick::Image.read("#{Rails.root}/public/play_arrow@3x.png").first
        dst = Magick::Image.read(@file.path).first
        src.resize_to_fit!(dst.columns-50, dst.rows-50)

        composite = dst.composite(src, Magick::CenterGravity, Magick::OverCompositeOp)
        
        composite.write(@file.path)
      end

      @file
    end
   end
 end